import { Sp17AngulardemoTovermierPage } from './app.po';

describe('sp17-angulardemo-tovermier App', () => {
  let page: Sp17AngulardemoTovermierPage;

  beforeEach(() => {
    page = new Sp17AngulardemoTovermierPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
