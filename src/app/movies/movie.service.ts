import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable'
import { Http, Response } from '@angular/http'
import { IMovie } from './movie'
import { IPagedResult } from './pagedResult'
import 'rxjs/add/operator/map';

@Injectable()
export class MovieService {

    private baseUrl: string = 'https://api.themoviedb.org/3';
    private api_key: string = 'ee6f4bbbf431841b19366a4368abe251';

    constructor(private http: Http) { }

    getMovies(page: number = 1): Observable<IPagedResult<IMovie>> {
        return this.http.get(`${this.baseUrl}/discover/movie?api_key=${this.api_key}&language=en-US&sort_by=popularity.desc&include_adult=false&include_video=false&page=${page}`)
            .map((response: Response) => <IPagedResult<IMovie>>response.json())
  }
}
