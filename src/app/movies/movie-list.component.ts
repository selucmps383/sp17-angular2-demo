import { Component, OnInit } from '@angular/core';
import { IMovie } from './movie'
import { MovieService } from './movie.service'

@Component({
    selector: 'app-movie-list',
    templateUrl: './movie-list.component.html',
    styleUrls: ['./movie-list.component.css']
})
export class MovieListComponent implements OnInit {
    currentPage: number;
    totalPages: number;
    totalResults: number;
    movies: IMovie[];

    constructor(private movieService: MovieService) { }

    ngOnInit() {
        this.updateMovies();
    }

    updateMovies(page: number = 1) {
        this.movieService.getMovies(page)
            .subscribe(response => {
                this.movies = response.results;
                this.currentPage = response.page;
                this.totalPages = response.total_pages;
                this.totalResults = response.total_results;
            })
    }

    setMovies(response) {
        this.movies = response.results;
    }

    prevPage() {
        if (this.currentPage > 1) {
            this.currentPage--;
            this.updateMovies(this.currentPage);
        }
    }

    nextPage() {
        this.currentPage++;
        this.updateMovies(this.currentPage);
    }
}
